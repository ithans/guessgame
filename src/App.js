import React from 'react'
import NewGame from "./NewGame";
import GuessName from './GuessName'

class App extends React.Component{

  render() {
    return(
      <div>
        <NewGame></NewGame>
        <GuessName/>
      </div>
    )
  }
}
export default App