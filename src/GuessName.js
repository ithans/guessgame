import React from 'react'
import Guess from "./Guess";
import Check from "./Check";

class GuessName extends React.Component{
  constructor(props){
    super(props);
    this.state={
      guessNum:[]
    }
  }
  handleChange(value){
    this.state.guessNum=[this.state.guessNum,value];
    console.log(this.state.guessNum);
  }

render() {
  return (
    <div>
      <Guess handleChange={this.handleChange}></Guess>
      <Guess handleChange={this.handleChange}></Guess>
      <Guess handleChange={this.handleChange}></Guess>
      <Guess handleChange={this.handleChange}></Guess>
      <Check></Check>
      <button>Guess</button>
    </div>
  );
}

}
export default GuessName