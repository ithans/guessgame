import React from 'react'

class Guess extends React.Component {

  handleChange(even){
    const value=even.target.value;
    this.props.handleChange(value);
  }
  render() {
    return (
        <input type="num" maxLength="1" onChange={this.handleChange.bind(this)}/>
    )
  }
}

export default Guess